const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 3030;
const mongo = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
let conn;

if (app.get('env') === 'development') {
    const cors = require('cors');
    app.use(cors());
    require('dotenv').config();
}

app.use(bodyParser.json());

MongoClient.connect(process.env.DB_HOST, function(err, db) {
    console.log("Connected successfully to db");
    conn = db;
});

app.use(express.static(path.join(__dirname, 'build')));

app.get('/budgets', function (req, res) {
    conn.collection('budgets').find({}).toArray((err, docs) => {
        res.json(docs);
    });
});

app.get('/budgets/:id', function (req, res) {
    conn.collection('budgets').findOne({ _id: new mongo.ObjectID(req.params.id) }, (err, doc) => {
        if (err) res.status(500).send(err);
        res.send(doc);
    });
});

app.post('/budgets', function (req, res) {
    conn.collection('budgets').insertOne(req.body, function (err, result) {
        if (err) res.status(500).send(err);
        res.status(200).json(result.ops[0]);
    }); 
});

app.put('/budgets/:id', function (req, res) {
    conn.collection('budgets').updateOne({
        _id: new mongo.ObjectID(req.params.id),
    }, { 
        $push: { purchases: req.body } 
    }, function (err, result) {
        if (err) res.status(500).send(err);
        res.sendStatus(200);
    });
});

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname+'/build/index.html'));
});

app.listen(process.env.PORT || 3030, function () {
    console.log(`${String.fromCodePoint(0x1F415)}  Pixel\'s Budget Tracker running on port ${port}! ${String.fromCodePoint(0x1F436)}`);
});

function cleanup () {
    console.log('Done cleanup up');
    conn.close();
    process.exit();
}

process.on('exit', cleanup);
process.on('SIGINT', cleanup);