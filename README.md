# Pixel's Budget Tracker
A simple web application for budget tracking. :dog:

# Notes
1. In a terminal, start the server with `npm start`
1. In a different terminal, start the UI with `npm run create-react-app-start`
1. Set `src/config.js` to development when working locally. Do not check the changes in.
1. Make sure to run `npm build` before deploying to production or the changes won't show up in Heroku!
