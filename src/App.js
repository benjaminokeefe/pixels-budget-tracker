import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import './App.css';
import Button from './Button';
import Select from './Select';
import Budget from './Budget';
import Stats from './Stats';
import NewBudget from './NewBudget';
import pix from './images/pix-baby.jpg';
import { sortByDate } from './utils.js';
import request from 'superagent';
import { config } from './config';
 
class App extends Component {
    constructor (props) {
        super(props);
        this.state = { budgets: [], selectedBudget: { name: '', purchases: [] }, showNewBudget: false }
      
        this.budgetSelectItemClick = this.budgetSelectItemClick.bind(this);
        this.newBudgetClick = this.newBudgetClick.bind(this);
        this.newBudgetItemClick = this.newBudgetItemClick.bind(this);
        this.onNewBudget = this.onNewBudget.bind(this);
        this.onNewBudgetCancel = this.onNewBudgetCancel.bind(this);
    }

    componentDidMount () {
        let endpoint = '/budgets';

        if (config.env === 'development') {
            endpoint = `http://127.0.0.1:3030${endpoint}`;
        }

        request
            .get(endpoint)
            .end((err, result) => {
                const budgets = result.body.sort(sortByDate('dateCreated')).map((budget, i) => {
                    budget.status = i === 0 ? 'Active' : 'Closed';
                    return budget;
                });

                this.setState({ budgets: budgets, selectedBudget: budgets[0] });
            });
    }

    budgetSelectItemClick (budget) {
        this.setState({ selectedBudget: this.state.budgets.find(b => b._id === budget._id) });
    }

    newBudgetClick () {
        this.setState({ showNewBudget: true, showNewBudgetItem: false });
    }

    newBudgetItemClick () {
        this.setState({ showNewBudget: false, showNewBudgetItem: true });
    }

    onNewBudget (budget) {
        let budgets = this.state.budgets.slice();
        budgets.forEach(budget => budget.status = 'Closed');
        budgets.push(budget);
        budgets = budgets.sort(sortByDate('dateCreated'));
        this.setState({ budgets: budgets, selectedBudget: budgets[0], showNewBudget: false });
    }

    onNewBudgetCancel () {
        this.setState({ showNewBudget: false });
    }

    render () {
        return (
            <div className="App">
                <Router>
                    <div>
                        <header className="App-header">
                            <Link className="App-header-link" to="/" style={{textDecoration:'none', color:'white'}}>
                                <h1 className="App-header-text">Pixel's Budget Tracker</h1>
                            </Link>
                            <img className="App-header-image" src={pix} alt="Pixel" />
                        </header>
                    
                        <main className="App-main">
                            <Route exact path="/" render={routeProps => (
                                <div className="Tracker">
                                    <section className="top-row">
                                        {this.state.budgets.length > 0 &&
                                        <Select items={this.state.budgets} selectedItem={this.state.selectedBudget} 
                                            onItemClick={this.budgetSelectItemClick} />
                                        }
                                        <Button text="New Budget" onClick={this.newBudgetClick} />
                                    </section>
                                    {this.state.showNewBudget &&
                                        <NewBudget onCancel={this.onNewBudgetCancel} onNewBudget={this.onNewBudget} />
                                    }
                                    {this.state.budgets.length > 0 &&
                                    <Budget newBudgetItemClick={this.newBudgetItemClick} showNewBudgetItem={this.state.showNewBudgetItem} 
                                        budget={this.state.selectedBudget} />
                                    }
                                </div>
                            )} />
                            <Route exact path="/:budgetName/stats" render={() => <Stats budget={this.state.selectedBudget} />} />
                        </main>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;
