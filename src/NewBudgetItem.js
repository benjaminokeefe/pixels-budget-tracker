import React, { Component } from 'react';
import './NewBudgetItem.css';
import Button from './Button';
import Select from './Select';

const itemTypes = [
    { _id: 'food', name: 'Food' },
    { _id: 'books', name: 'Books' },
    { _id: 'clothes', name: 'Clothes' },
    { _id: 'electronics', name: 'Electronics' },
    { _id: 'home', name: 'Home' },
    { _id: 'medicine', name: 'Medicine' },
    { _id: 'other', name: 'Other' },
    { _id: 'pet', name: 'Pet' },
    { _id: 'recreation', name: 'Recreation' },
    { _id: 'restaurant', name: 'Restaurant' },
    { _id: 'transportation', name: 'Transportation' },
    { _id: 'travel', name: 'Travel' }
];

class NewBudgetItem extends Component {
    constructor (props) {
        super(props);
        this.state = { name: '', price: '', itemTypes, selectedItem: itemTypes[0], disableAddButton: true, buyer: 'ben' };

        this.onItemClick = this.onItemClick.bind(this);
        this.addClick = this.addClick.bind(this);
        this.nameInputChangeHandler = this.nameInputChangeHandler.bind(this);
        this.priceInputChangeHandler = this.priceInputChangeHandler.bind(this);
        this.validateNewBudgetItem = this.validateNewBudgetItem.bind(this);
        this.whoRadioButtonChangeHandler = this.whoRadioButtonChangeHandler.bind(this);
    }

    onItemClick (itemType) {
        this.setState({ selectedItem: itemType });
    }

    addClick () {
        const newBudgetItem = {
            datePurchased: new Date(),
            name: this.state.name,
            price: parseFloat(this.state.price),
            category: this.state.selectedItem.name,
            buyer: this.state.buyer
        }
        this.setState({ name: '', price: '', disableAddButton: true, selectedItem: itemTypes[0] });
        this.props.onNewBudgetItem(newBudgetItem);
    }

    nameInputChangeHandler (e) {
        this.setState({ name: e.target.value }, () => {
            this.validateNewBudgetItem();
        });
    }

    priceInputChangeHandler (e) {
        const value = e.target.value;
        if (isNaN(value)) return;

        this.setState({ price: value }, () => {
            this.validateNewBudgetItem();
        });
    }

    whoRadioButtonChangeHandler (e) {
        this.setState({ buyer: e.target.value });
    }

    validateNewBudgetItem () {
        if (this.state.name && this.state.price) {
            this.setState({ disableAddButton: false });
        } else {
            this.setState({ disableAddButton: true });
        }
    }

    render () {
        return (
             <section className={"NewBudgetItem"}>
                <header>Add New Budget Item</header>

                <div className="NewBudgetItem-input-row">
                    <div className="who-wrapper">
                        <div className="who-item">
                            <label htmlFor="ben">Ben</label>
                            <input type="radio" name="who" id="ben" value="ben" onChange={this.whoRadioButtonChangeHandler}
                                checked={this.state.buyer === 'ben'} />
                        </div>
                        <div className="who-item">
                            <label htmlFor="kana">Kana</label>
                            <input type="radio" name="who" id="kana" value="kana" onChange={this.whoRadioButtonChangeHandler}
                                checked={this.state.buyer === 'kana'} />
                        </div>
                    </div>
                    <input autoFocus type="text" value={this.state.name} onChange={this.nameInputChangeHandler} placeholder="Name" />
                    <input type="text" value={this.state.price} onChange={this.priceInputChangeHandler} placeholder="Price" />
                    <Select items={this.state.itemTypes} selectedItem={this.state.selectedItem} onItemClick={this.onItemClick} />
                    <div className="NewBudgetItem-buttons">
                        <Button text="Cancel" onClick={this.props.onNewBudgetItemCancel} />
                        <Button text="Add" onClick={this.addClick} disabled={this.state.disableAddButton} />
                    </div>
                </div>
            </section>
        );
    }
}

export default NewBudgetItem;
