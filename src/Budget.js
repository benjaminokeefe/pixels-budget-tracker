import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Budget.css';
import NewBudgetItem from './NewBudgetItem';
import Button from './Button.js';
import { sortByDate, sortByString, sortByNumber, formatDate } from './utils.js';
import request from 'superagent';
import { config } from './config';

class BudgetItem extends Component {
    render () {
        return (
            <li className={"Budget-item " + (this.props.purchase.buyer)}>
                <div className="Budget-item-date">{formatDate(this.props.purchase.datePurchased)}</div>
                <div className="Budget-item-details">
                    <div className="Budget-item-name">{this.props.purchase.name}</div>
                    <div className="Budget-item-category">{this.props.purchase.category}</div>
                </div>
                <div className="Budget-item-price">${this.props.purchase.price.toFixed(2)}</div>
            </li>
        );
    }
}

class Budget extends Component {
    constructor (props) {
        super(props);

        this.state = {
            budget: this.props.budget,
            purchases: this.props.budget.purchases.sort(sortByDate('datePurchased')),
            showNewBudgetItem: false
        }

        this.sortByClick = this.sortByClick.bind(this);
        this.onNewBudgetItem = this.onNewBudgetItem.bind(this);
        this.onNewBudgetItemCancel = this.onNewBudgetItemCancel.bind(this);
        this.newBudgetItemClick = this.newBudgetItemClick.bind(this);
        this.getAmountSpent = this.getAmountSpent.bind(this);
        this.getAmountSpentClass = this.getAmountSpentClass.bind(this);
    }

    getAmountSpent (purchases) {
        return purchases.reduce((totalAmountSpent, purchase) => {
            return totalAmountSpent + purchase.price; 
        }, 0).toFixed(2);
    }

    getAmountSpentClass (amountSpent, purchaseLimit) {
        const amountSpentFloat = parseFloat(amountSpent);
        const purchaseLimitFloat = parseFloat(purchaseLimit);
        const warningThreshold = purchaseLimit - (purchaseLimitFloat / 3);

        if (amountSpentFloat >= warningThreshold && amountSpentFloat <= purchaseLimit) return 'warning';
        if (amountSpentFloat < purchaseLimitFloat) return 'under';
        return 'over';
    }

    sortByClick (e) {
        const text = e.target.textContent;
        let sortFunction;
        
        if (text === 'date') {
            sortFunction = sortByDate('datePurchased');
        } else if (text === 'type') {
            sortFunction = sortByString('category');
        } else if (text === 'price') {
            sortFunction = sortByNumber('price');
        } else if (text === 'buyer') {
            sortFunction = sortByString('buyer');
        }

        this.setState({
            purchases: this.state.purchases.sort(sortFunction)
        });
    }

    componentWillReceiveProps (props) {
        this.setState({
            budget: props.budget,
            purchases: props.budget.purchases.sort(sortByDate('datePurchased')),
            showNewBudgetItem: props.showNewBudgetItem
        });
    }

    newBudgetItemClick () {
        this.setState({ showNewBudgetItem: true });
        this.props.newBudgetItemClick();
    }

    onNewBudgetItem (budgetItem) {
        const budget = this.state.budget;
        let endpoint = `/budgets/${budget._id}`;

        if (config.env === 'development') {
            endpoint = `http://127.0.0.1:3030${endpoint}`;
        }

        budget.purchases.unshift(budgetItem);

        request
            .put(endpoint)
            .send(budgetItem)
            .end((err, result) => {
                if (err) console.error(err);
            });

        budgetItem.datePurchased = formatDate(budgetItem.datePurchased);
        this.setState({ budget: budget, showNewBudgetItem: false });
    }

    onNewBudgetItemCancel () {
        this.setState({ showNewBudgetItem: false });
    }

    render () {
        const purchaseLimit = parseFloat(this.state.budget.purchaseLimit);
        let purchases = null;
        let amountSpent = 0;
        let amountSpentClass = '';

        if (this.state.purchases.length) {
            purchases = this.state.purchases.map((purchase, i) => {
                return <BudgetItem purchase={purchase} key={i} />
            });
            
            amountSpent = this.getAmountSpent(this.state.purchases);
            amountSpentClass = this.getAmountSpentClass(amountSpent, purchaseLimit);
        }

        return (
            <article className="Budget">
                <header className="Budget-header">
                    <h1>
                        {this.state.budget.name}
                        <span className={"Budget-status " + (this.state.budget.status === 'Active' ? 
                            'active' : 'closed') }>({this.state.budget.status})</span>
                    </h1>
                    <Link 
                        className="Button" 
                        to={`/${this.state.budget.name}/stats`}
                        style={{textDecoration:'none', textAlign:'center', color:'black'}}
                    >
                        View Stats
                    </Link>
                    {this.state.budget.status === 'Active' &&
                        <Button text="New Item" onClick={this.newBudgetItemClick} />
                    }
                </header>
                {this.state.showNewBudgetItem &&
                    <NewBudgetItem onNewBudgetItem={this.onNewBudgetItem} onNewBudgetItemCancel={this.onNewBudgetItemCancel} />
                }
                <section className="Budget-details">
                    <label>Limit:</label>
                    <span className="limit">${this.state.budget.purchaseLimit}</span>

                    <label>Spent:</label>
                    <span className={amountSpentClass + " spent"}>${amountSpent}</span>
                </section>
                <section className="Budget-sort">
                    Sort By:
                    <span className="Budget-sort-by date" onClick={this.sortByClick}>date</span>
                    <span className="Budget-sort-by type" onClick={this.sortByClick}>type</span>
                    <span className="Budget-sort-by price" onClick={this.sortByClick}>price</span>
                    <span className="Budget-sort-by buyer" onClick={this.sortByClick}>buyer</span>
                </section>
                <ul className="Budget-items">
                    {purchases}
                </ul>
            </article>
        );
    }
}

export default Budget
