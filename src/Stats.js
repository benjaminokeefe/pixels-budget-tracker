import React from 'react';
import { PieChart, Pie, Cell, Legend } from 'recharts';
import './Stats.css';

class Stats extends React.Component {
  getNumberOfPurchases () {
    return this.props.budget.purchases.length;
  }

  getNumberOfPurchasesByBuyer (buyer) {
    return this.props.budget.purchases.filter(b => b.buyer === buyer).length;
  }

  getTotalOfPurchasesByBuyer (buyer) {
    return this.props.budget.purchases.reduce((total, purchase) => {
      if (purchase.buyer === buyer) {
        total += purchase.price
      }
      return total;
    }, 0).toFixed(2);
  }

  getMostExpensivePurchases () {
    const purchasesClone = Array.from(this.props.budget.purchases);

    purchasesClone.sort((a, b) => {
      return b.price - a.price;
    });

    console.log(purchasesClone);
    return purchasesClone;
  }

  getCategoryCountMap () {
    const categoryMap = {};
    
    this.props.budget.purchases.forEach(purchase => {
      if (!categoryMap[purchase.category]) {
        categoryMap[purchase.category] = 1;
      } else {
        categoryMap[purchase.category] = categoryMap[purchase.category] += 1;
      }
    });

    return categoryMap;
  }

  getNumberOfPurchasesByCategory () {
    const categoryMap = this.getCategoryCountMap();

    return Object.keys(categoryMap).map(key => {
      return {
        name: key,
        value: categoryMap[key]
      };
    });
  }

  getTotalSpent () {
    return this.props.budget.purchases.reduce((total, purchase) => {
      total += purchase.price;
      return total;
    }, 0).toFixed(2);
  }

  render () {
    const numberOfPurchasesByCategory = this.getNumberOfPurchasesByCategory();
    const mostExpensivePurchases = this.getMostExpensivePurchases().slice(0, 3);
    const COLORS = [
      'red', 'green', 'blue', 'orange',
      'turquoise', 'purple', 'goldenrod', 'pink'
    ];

    return (
      <section className="Stats">
        <h1>Items</h1>
        {this.props.budget ? (
          <div className="stats">
            <div className="row items">
              <div className="stat">
                <label>Total</label> 
                <div className="value">{this.getNumberOfPurchases()}</div>
              </div>

              <div className="stat">
                <label>Ben</label>
                <div className="value" style={{color:'lightblue'}}>{this.getNumberOfPurchasesByBuyer('ben')}</div>
              </div>

              <div className="stat">
                <label>Kana</label>
                <div className="value" style={{color:'pink'}}>{this.getNumberOfPurchasesByBuyer('kana')}</div>
              </div>
            </div>

            <h1>Spent</h1>
            <div className="row spent">
              <div className="stat">
                <label>Total</label>
                <div className="value">{`$${this.getTotalSpent()}`}</div>
              </div>

              <div className="stat">
                <label>Ben</label>
                <div className="value" style={{color:'lightblue'}}>{`$${this.getTotalOfPurchasesByBuyer('ben')}`}</div>
              </div>

              <div className="stat">
                <label>Kana</label>
                <div className="value" style={{color:'pink'}}>{`$${this.getTotalOfPurchasesByBuyer('kana')}`}</div>
              </div>
            </div>
            
            <div className="row most-expensive">
              <h1>Most Expensive</h1>
              <table>
                <tbody>
                  {mostExpensivePurchases.map((purchase, index) => (
                    <tr key={purchase.price} className={`value ${purchase.buyer}`}>
                      <td>{purchase.name}</td>
                      <td>${purchase.price}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>

            <div className="row chart">
              <h1>Categories</h1>
              <PieChart width={300} height={300}>
                <Pie 
                  dataKey="value"
                  dataName="name"
                  data={numberOfPurchasesByCategory}
                  fill="#82ca9d"
                  isAnimationActive={false}
                  legendType="rect" 
                  label
                >
                {
                  numberOfPurchasesByCategory.map((entry, index) => 
                    <Cell key={entry} fill={COLORS[index % COLORS.length]}/>
                  )
                }
                </Pie>
                <Legend iconType="circle" align="left" iconSize={15} align="center" verticalAlign="bottom" />
              </PieChart>
            </div>
          </div>
        ) : (
          <p>Loading...</p>
        )}
      </section>
    );
  }
}

export default Stats;
