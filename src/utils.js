export function sortByDate (sortProperty) {
    return function (a, b) {
        return new Date(b[sortProperty]) - new Date(a[sortProperty]);
    }
}

export function sortByString (sortProperty) {
    return function (a, b) {
        const 
            stringA = a[sortProperty].toUpperCase(),
            stringB = b[sortProperty].toUpperCase();

        if (stringA < stringB) { return -1; }
        if (stringA > stringB) { return 1; }
        return 0;
    }
}

export function sortByNumber (sortProperty) {
    return function sortByNumber (a, b) {
        return b[sortProperty] - a[sortProperty];
    }
}

export function formatDate (date) {
    const temp = new Date(date);
    let month = temp.getMonth() + 1;
    let day = temp.getDate();
    let year = temp.getFullYear();

    if (month < 10) month = `0${month}`;
    if (day < 10) day = `0${day}`;

    return `${month}.${day}.${year}`;
}

export function getSelectedRadioButton (radioButtonGroup){
    for (var i = 0; i < radioButtonGroup.length; i++) {
        if (radioButtonGroup[i].checked) {
            return i;
        }
    }
    return 0;
}