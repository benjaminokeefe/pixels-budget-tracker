import React from 'react';
import ReactDOM from 'react-dom';
import Budget from './Budget';

const budget = {
    "id": 1,
    "name": "Budget 1",
    "purchases": [
        {
            "datePurchased": "6.24.2017",
            "name": "Galaxy S8",
            "price": "$427"
        },
        {
            "datePurchased": "6.24.2017",
            "name": "Tums",
            "price": "$4.50"
        },
        {
            "datePurchased": "6.25.2017",
            "name": "McDonald's",
            "price": "$10.85"
        },
        {
            "datePurchased": "6.26.2017",
            "name": "Bus fare",
            "price": "$2.75"
        },
        {
            "datePurchased": "6.28.2017",
            "name": "Reefer",
            "price": "$37"
        },
        {
            "datePurchased": "6.28.2017",
            "name": "Vitamin Water",
            "price": "$1.99"
        }
    ]
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Budget budget={budget} />, div);
});