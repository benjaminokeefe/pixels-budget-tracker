import React from 'react';
import ReactDOM from 'react-dom';
import Button from './Button';

const onClick = function () {};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Button onClick={onClick} />, div);
});