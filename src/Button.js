import React, { Component } from 'react';
import './Button.css';

class Button extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.props.onClick.bind(this);
    }

    render () {
        return (
            <button className="Button" onClick={this.onClick} disabled={this.props.disabled}>
                {this.props.text}
            </button>
        );
    }
}

export default Button;