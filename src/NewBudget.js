import React, { Component } from 'react';
import './NewBudget.css';
import Button from './Button';
import request from 'superagent';
import { config } from './config';

class NewBudget extends Component {
    constructor (props) {
        super(props);
        this.state = { name: '', purchaseLimit: '', disableAddButton: true };

        this.addClick = this.addClick.bind(this);
        this.cancelClick = this.cancelClick.bind(this);
        this.nameInputChangeHandler = this.nameInputChangeHandler.bind(this);
        this.purchaseLimitInputChangeHandler = this.purchaseLimitInputChangeHandler.bind(this);
        this.validateNewBudget = this.validateNewBudget.bind(this);
    }

    addClick () {
        const budget = this.getNewBudget(this.state.name, this.state.purchaseLimit);
        let endpoint = '/budgets';

        if (config.env === 'development') {
            endpoint = `http://127.0.0.1:3030${endpoint}`;
        }

        request
            .post(endpoint)
            .send(budget)
            .end((err, result) => {
                if (err) console.error(err);
                this.props.onNewBudget(JSON.parse(result.text));
            });
        this.setState({ name: '', purchaseLimit: '', disableAddButton: true });
    }
    
    cancelClick () {
        this.setState({ name: '', purchaseLimit: '', disableAddButton: true });
        this.props.onCancel();
    }

    nameInputChangeHandler (e) {
        this.setState({ name: e.target.value }, () => {
            this.validateNewBudget();
        });
    }

    purchaseLimitInputChangeHandler (e) {
        const value = e.target.value;
        if (isNaN(value)) return;

        this.setState({ purchaseLimit: value }, () => {
            this.validateNewBudget();
        });
    }

    getNewBudget (name, purchaseLimit) {
        return {
            name: name,
            status: 'Active',
            dateCreated: new Date(),
            purchaseLimit: purchaseLimit,
            purchases: []
        };
    }

    validateNewBudget () {
        if (this.state.name && this.state.purchaseLimit) {
            this.setState({ disableAddButton: false });
        } else {
            this.setState({ disableAddButton: true });
        }
    }

    render () {
        return (
            <section className="NewBudget">
                <header>Add New Budget</header>
                <span className="NewBudget-warning"><span className="warning">WARNING:</span> The current budget will be closed when a new budget is added.</span>

                <div className="NewBudget-input-row">
                    <input autoFocus type="text" value={this.state.name} onChange={this.nameInputChangeHandler} placeholder="Name" />
                    <input type="text" value={this.state.purchaseLimit} onChange={this.purchaseLimitInputChangeHandler} placeholder="Purchase Limit" />
                    <div className="NewBudget-buttons">
                        <Button text="Cancel" onClick={this.cancelClick} />
                        <Button text="Add" onClick={this.addClick} disabled={this.state.disableAddButton} />
                    </div>
                </div>
            </section>
        );
    }
}

export default NewBudget;