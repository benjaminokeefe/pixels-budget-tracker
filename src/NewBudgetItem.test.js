import React from 'react';
import ReactDOM from 'react-dom';
import NewBudgetItem from './NewBudgetItem';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NewBudgetItem />, div);
});