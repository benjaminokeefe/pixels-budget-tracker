import React from 'react';
import ReactDOM from 'react-dom';
import NewBudget from './NewBudget';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NewBudget />, div);
});