import React, { Component } from 'react';
import './Select.css';

 class SelectItem extends Component {
    render () {
        return (
            <li className="SelectItem" onClick={this.props.onClick.bind(null, this.props.item)}>
                {this.props.item.name}
            </li>
        );
    }
}

class Select extends Component {
    constructor(props) {
        super(props);
        this.state = { showMenu: false };
        this.onClick = this.onClick.bind(this);
    }

    onClick () {
        this.setState({ showMenu: !this.state.showMenu });
    }

    render () {
        return (
            <div className={"Select " + (this.state.showMenu ? "open" : "closed")} onClick={this.onClick}>
                <div className="Select-selected-item">
                    <div className="Select-selected-item-text">{this.props.selectedItem.name}</div>
                    <div className="Select-selected-item-arrow">&#9650;</div>
                </div>
                <ul className="Select-items">
                    {this.props.items.map((item, i) => {
                        return item._id !== this.props.selectedItem._id ?
                            <SelectItem item={item} key={i} onClick={this.props.onItemClick} /> : null;
                    })}
                </ul>
            </div>
        );
    }
}

export default Select;